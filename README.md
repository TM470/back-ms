# BackMs

This software is part of prototype built for the The Open University's Module TM470.

Welcome to the backend service for ScanApp and BackApp.

## How to start the BackMs application

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/back-ms-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

## Firebase private key

The Firebase private key is not shipped along with source code for obvious security reasons.
However, the service is designed to work even is that file is not present. Of course, all the functionalities
concerning tpush notifications will not be provided.

If you want to generate a Firebase private key, please follow the Google Firebase Cloud
Messaging [guide](https://firebase.google.com/docs/admin/setup).

Then place the file `firebase_private_key.json` in the root of the project.

## License

The source code is released under the Apache License v2.0

## Author

Alessandro Pieri, 2018