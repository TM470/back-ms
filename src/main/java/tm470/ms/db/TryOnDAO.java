/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.db;

import tm470.ms.api.Status;
import tm470.ms.api.TryOn;

import java.util.List;

/**
 * Data Access Object for operation on try on.
 */
public interface TryOnDAO {

    /**
     * Add a new try on request.
     * @param tryOn a new Try to add
     * @return The incoming request including the request ID
     */
    TryOn addTryOn(TryOn tryOn);

    /**
     * Get try-on requests filtered by the given status.
     * @param matchingStatus Status to filter out
     * @return Collection of TryOn requests
     */
    List<TryOn> getTryOns(Status matchingStatus);

    /**
     * Return the TryOn request matching the given ID.
     * @param id TryOn request ID
     * @return TryOn request
     */
    TryOn getTryOn(String id);

    /**
     * Update the status of the TryOn request.
     * @param id TryOn request id
     * @param newStatus New status
     * @return TryOn request with the updated status
     */
    TryOn updateTryOnStatus(String id, Status newStatus);
}
