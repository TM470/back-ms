/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.db;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import tm470.ms.api.Product;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class ProductDAOImpl implements ProductDAO {

    private final ObjectMapper objectMapper;
    private final Set<Product> products;

    public ProductDAOImpl(final ObjectMapper objectMapper, final String filename) {
        this.objectMapper = objectMapper;
        try {
            products = objectMapper.readValue(new File(filename), new TypeReference<HashSet<Product>>() { });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Product> getById(String productId) {
        return this.products.stream().filter(p -> p.getId().equalsIgnoreCase(productId)).findFirst();
    }

    @Override
    public synchronized void deleteProduct(String productId) {
        products.remove(new Product(productId));
    }
}
