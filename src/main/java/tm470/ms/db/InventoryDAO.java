/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.db;

import tm470.ms.api.Size;

import java.util.Optional;
import java.util.Set;

/**
 * Data Access Object for inventory operations.
 */
public interface InventoryDAO {

    /**
     * Get size and quantity by shop and product IDs.
     * @param shopId Shop identifier
     * @param productId Product identifier
     * @return Size and quantity
     */
    Set<Size> getAvailableSizes(String shopId, String productId);

    /**
     * Get the quantity in stock of a given size.
     * @param shopId Shop identifier
     * @param productId Product identifier
     * @param size Size name
     * @return Quantity
     */
    Optional<Integer> getSizeQuantity(String shopId,
                                      String productId,
                                      String size);

    /**
     * Update the quantity in stock of a given size.
     * @param shopId Shop identifier
     * @param productId Product identifier
     * @param size Size name
     * @param quantity new quantity
     */
    void updateQuantity(String shopId,
                        String productId,
                        String size,
                        int quantity);

    /**
     * Delete an item from the stock.
     * @param shopId Shop identifier
     * @param productId Product identifier
     * @return True if operation succeeded
     */
    Boolean deleteItem(String shopId, String productId);
}
