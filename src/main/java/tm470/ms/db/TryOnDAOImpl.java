/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.db;

import com.google.common.collect.ImmutableList;
import tm470.ms.api.Status;
import tm470.ms.api.TryOn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TryOnDAOImpl implements TryOnDAO {

    private Map<String, TryOn> tryOns = new HashMap<>();
    private AtomicInteger tryOnCount = new AtomicInteger(0);

    @Override
    public synchronized TryOn addTryOn(TryOn tryOn) {
        tryOn.setId(String.format("%03d", tryOnCount.incrementAndGet()));
        tryOns.put(tryOn.getId(), tryOn);
        return tryOn;
    }

    @Override
    public List<TryOn> getTryOns(Status matchingStatus) {
        ImmutableList.Builder<TryOn> listBuilder = new ImmutableList.Builder<>();
        for (TryOn tryOn : this.tryOns.values()) {
            if (tryOn.getStatus().equals(matchingStatus)) {
                listBuilder.add(tryOn);
            }
        }
        return listBuilder.build();
    }

    @Override
    public TryOn getTryOn(String id) {
        return tryOns.get(id);
    }

    @Override
    public synchronized TryOn updateTryOnStatus(String id, Status newStatus) {
        TryOn foundTryOn = this.tryOns.get(id);
        if (null != foundTryOn) {
            foundTryOn.setStatus(newStatus);
        }
        return foundTryOn;
    }
}
