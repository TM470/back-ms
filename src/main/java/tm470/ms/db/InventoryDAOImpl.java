/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.db;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import tm470.ms.api.Inventory;
import tm470.ms.api.ShopInventory;
import tm470.ms.api.Size;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class InventoryDAOImpl implements InventoryDAO {

    private final ObjectMapper objectMapper;
    private final Map<String, Set<Inventory>> inventoryMap;

    public InventoryDAOImpl(final ObjectMapper objectMapper, final String filename) {
        this.objectMapper = objectMapper;
        try {
            List<ShopInventory> shopInventory = objectMapper.readValue(new File(filename), new TypeReference<List<ShopInventory>>() {
            });
            inventoryMap = buildMap(shopInventory);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<String, Set<Inventory>> buildMap(List<ShopInventory> shopInventory) {
        Map<String, Set<Inventory>> inventoryMap = new HashMap<>();
        for (ShopInventory inventory : shopInventory) {
            inventoryMap.put(inventory.getId(), inventory.getInventory());
        }
        return inventoryMap;
    }

    private Optional<Size> getSize(String shopId, String productId, String size) {
        Set<Size> availableSizes = getAvailableSizes(shopId, productId);
        if (!availableSizes.isEmpty()) {
            return availableSizes.stream().filter(s -> s.getSize().equalsIgnoreCase(size)).findFirst();
        }
        return Optional.empty();
    }

    @Override
    public Set<Size> getAvailableSizes(String shopId, String productId) {
        Optional<Set<Inventory>> shopInventories = Optional.ofNullable(inventoryMap.get(shopId));
        if (shopInventories.isPresent()) {
            Optional<Inventory> inventory = shopInventories.get().stream().filter(p -> p.getProductId().equalsIgnoreCase(productId)).findFirst();
            if (inventory.isPresent()) {
                return inventory.get().getSizesInventory();
            }
        }
        return Collections.emptySet();
    }

    @Override
    public Optional<Integer> getSizeQuantity(String shopId, String productId, String size) {
        Optional<Size> foundSize = getSize(shopId, productId, size);
        if (foundSize.isPresent()) {
            return Optional.ofNullable(foundSize.get().getQuantity());
        }
        return Optional.empty();
    }

    @Override
    public void updateQuantity(String shopId, String productId, String size, int quantity) {
        Optional<Size> sizeFound = getSize(shopId, productId, size);
        if (sizeFound.isPresent()) {
            sizeFound.get().setQuantity(quantity);
        }
    }

    @Override
    public Boolean deleteItem(String shopId, String productId) {
        Optional<Set<Inventory>> shopInventories = Optional.ofNullable(inventoryMap.get(shopId));
        if (shopInventories.isPresent()) {
            Inventory inventory = shopInventories.get().stream().filter(p -> p.getProductId().equalsIgnoreCase(productId)).findFirst().get();
            return inventoryMap.get(shopId).remove(inventory);
        }
        return Boolean.FALSE;
    }
}
