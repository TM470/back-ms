/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.db;

/**
 * Data Access Object for device operations.
 */
public interface DeviceDAO {

    /**
     * Register or update a device token.
     * @param deviceUUID Obfuscated device UUID
     * @param token Firebase identification token
     * @return Input token
     */
    void save(String deviceUUID, String token);

    /**
     * Get the Firebase token ID matching the given device UUID.
     * @param deviceUUID Obfuscated device UUID
     * @return Token
     */
    String getToken(String deviceUUID);

}
