/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Size {

    /**
     * Product size.
     */
    private String size = null;

    /**
     * Number of units in stock.
     */
    private int quantity = 0;

    @JsonCreator
    public Size(@JsonProperty("size") String size, @JsonProperty("quantity") int quantity) {
        if (null == size) {
            throw new IllegalArgumentException("Size cannot be null.");
        }
        this.size = size;
        this.quantity = quantity;
    }

    public String getSize() {
        return size;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Size size1 = (Size) o;

        return size.equals(size1.size);

    }

    @Override
    public int hashCode() {
        return size.hashCode();
    }
}
