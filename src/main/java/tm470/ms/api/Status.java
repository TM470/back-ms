/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.api;

/**
 * Status of the TryOn request.
 */
public enum Status {

    /**
     * A new request has just come.
     */
    INCOMING,

    /**
     * The request in under progress.
     */
    IN_PROGRESS,

    /**
     * The request items have been delivered.
     */
    DELIVERED,

    /**
     * Items are not available or the request cannot
     * be processed for whatever reason.
     */
    REJECTED
}
