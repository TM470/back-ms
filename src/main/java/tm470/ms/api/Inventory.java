/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

/**
 * Entity that wrap the product and its availability.
 */
public class Inventory {

    /**
     * Item identifier.
     */
    private String productId;

    /**
     * List of available sizes and their quantities.
     */
    private Set<Size> sizesInventory;

    @JsonCreator
    public Inventory(@JsonProperty("productId") String productId,
                     @JsonProperty("sizesInventory") Set<Size> sizesInventory) {
        this.productId = productId;
        this.sizesInventory = sizesInventory;
    }

    public Set<Size> getSizesInventory() {
        return sizesInventory;
    }

    public void setSizesInventory(Set<Size> sizesInventory) {
        this.sizesInventory = sizesInventory;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inventory inventory = (Inventory) o;

        return productId != null ? productId.equals(inventory.productId) : inventory.productId == null;
    }

    @Override
    public int hashCode() {
        return productId != null ? productId.hashCode() : 0;
    }
}
