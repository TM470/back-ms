/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Product entity.
 */
public class Product {

    /**
     * Product unique identifier.
     */
    private String id = null;

    /**
     * Brand name.
     */
    private String brand = null;

    /**
     * Product name.
     */
    private String name = null;

    /**
     * Product description.
     */
    private String description = null;

    /**
     * Stock availability.
     */
    private Set<Size> availableSizes = new HashSet<>();

    /**
     * URL of the main product's picture.
     */
    private String photo = null;

    /**
     * Final price.
     */
    private BigDecimal price = null;

    @JsonCreator
    public Product(@JsonProperty("id") String id) {
        if (null == id) {
            throw new IllegalArgumentException("ID cannot be null!");
        }
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Set<Size> getAvailableSizes() {
        return availableSizes;
    }

    public void setAvailableSizes(Set<Size> availableSizes) {
        this.availableSizes = availableSizes;
    }

    public void addAvailableSizes(Size size) {
        this.availableSizes.add(size);
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return id.equals(product.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
