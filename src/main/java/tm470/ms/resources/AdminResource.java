/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import tm470.ms.db.InventoryDAO;

import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * REST API interface for Admin operations.
 * It allows to alter date in the datastore for management purpose.
 */
@Path("/admin")
@Produces(MediaType.APPLICATION_JSON)
public class AdminResource {

    private final InventoryDAO dao;

    public AdminResource(InventoryDAO dao) {
        this.dao = dao;
    }

    /**
     * Decrement by 1 unit the quantity of the product
     * identified by the given ID which size is the given size.
     * @param productId Unique product identifier
     * @param size Product size
     * @param shopId Shop ID*
     * @return True if succeeded.
     */
    @PUT
    @Path("/decrementQuantity/{id}")
    public Response decrementQuantity(@PathParam("id") String productId,
                                     @QueryParam("size") String size,
                                     @QueryParam("shopId") String shopId) {
        Optional<Integer> currentQuantity = dao.getSizeQuantity(shopId, productId, size);
        if (currentQuantity.isPresent()) {
            if (currentQuantity.get() > 0) {
                dao.updateQuantity(shopId, productId, size, currentQuantity.get() - 1);
                return Response.ok().build();
            } else {
                return Response.notModified().build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    /**
     * Delete product from the catalogue.
     * @param productId Product ID to delete
     * @param shopId Shop ID
     * @return True if successful deleted
     */
    @DELETE
    public Response deleteItem(@PathParam("id") String productId, @QueryParam("shopId") String shopId) {
        if (dao.deleteItem(shopId, productId)) {
            return Response.ok().build();
        }
        return Response.notModified().build();
    }
}
