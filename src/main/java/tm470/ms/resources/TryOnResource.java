/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import com.google.firebase.messaging.FirebaseMessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tm470.ms.api.ItemAvailability;
import tm470.ms.api.Product;
import tm470.ms.api.Status;
import tm470.ms.api.TryOn;
import tm470.ms.api.TryOnRequest;
import tm470.ms.core.NotificationBuilder;
import tm470.ms.db.DeviceDAO;
import tm470.ms.db.ProductDAO;
import tm470.ms.db.TryOnDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * REST API interface for try-on operations.
 */
@Path("/try-on")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TryOnResource {

    private final static Logger LOG = LoggerFactory.getLogger(TryOnResource.class);

    private final TryOnDAO tryOnDAO;
    private final ProductDAO productDAO;
    private final DeviceDAO deviceDAO;

    /**
     * Set up a new object with the given data store.
     * @param tryOnDAO
     */
    public TryOnResource(TryOnDAO tryOnDAO, ProductDAO productDAO, DeviceDAO deviceDAO) {
        this.tryOnDAO = tryOnDAO;
        this.productDAO = productDAO;
        this.deviceDAO = deviceDAO;
    }

    /**
     * Add a new try on request.
     * @param request Request.
     * @return The incoming request including the request ID.
     */
    @POST
    public TryOn makeRequest(TryOnRequest request) {
        TryOn tryOn = new TryOn();
        tryOn.setSizes(request.getSizes());
        tryOn.setDeviceUUID(request.getDeviceUUID());
        tryOn.setShopId(request.getShopId());

        Optional<Product> product = productDAO.getById(request.getProductId());
        if (product.isPresent()) {
            tryOn.setProduct(product.get());
        }

        return tryOnDAO.addTryOn(tryOn);
    }

    /**
     * Return the TryOn request matching the given ID and status.
     * @param id TryOn request ID
     * @param shopId ShopInventory ID
     * @param status Status of the requests. Default: INCOMING
     * @return TryOn requests matching the given input criteria
     */
    @GET
    public List<TryOn> getRequests(@QueryParam("id") String id, @QueryParam("shopId") String shopId, @DefaultValue("INCOMING") @QueryParam("status") Status status) {
        if (null != id) {
            return Collections.singletonList(tryOnDAO.getTryOn(id));
        }
        return tryOnDAO.getTryOns(status);
    }

    /**
     * Update the status of the TryOn request.
     * @param id TryOn request id
     * @param status New status
     * @return TryOn request with the updated status
     */
    @PUT
    @Path("/{id}")
    public Response changeStatus(@PathParam("id") String id, @QueryParam("status") Status status,
                                        List<ItemAvailability> itemAvailability) throws FirebaseMessagingException {
        TryOn tryOn = tryOnDAO.getTryOn(id);

        TryOn response = tryOnDAO.updateTryOnStatus(id, status);

        if (null != tryOn) {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.withId(id);
            notificationBuilder.withStatus(status);
            notificationBuilder.withItemAvailabilities(Optional.ofNullable(itemAvailability));
            notificationBuilder.withToken(deviceDAO.getToken(tryOn.getDeviceUUID()));
            notificationBuilder.build().send();
        }
        return Response.ok(response).build();
    }
}
