/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tm470.ms.db.DeviceDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST API interface for Device operations.
 */
@Path("/device")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DeviceResource {

    private final static Logger LOG = LoggerFactory.getLogger(DeviceResource.class);

    private final DeviceDAO dao;

    public DeviceResource(DeviceDAO dao) {
        this.dao = dao;
    }

    /**
     * Register or update a device woith its corresponding Firebase token.
     * @param uuid Obfuscated device UUID
     * @param token Firebase identification token
     * @return Result of the operation.
     */
    @PUT
    @Path("/{uuid}")
    public Response register(@PathParam("uuid") String uuid, @QueryParam("token") String token) {
        this.dao.save(uuid, token);
        return Response.ok().build();
    }
}
