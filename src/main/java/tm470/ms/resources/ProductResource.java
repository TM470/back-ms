/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import tm470.ms.api.Product;
import tm470.ms.db.InventoryDAO;
import tm470.ms.db.ProductDAO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * REST API interface for Product operations.
 */
@Path("/product")
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource {

    private final ProductDAO productDAO;
    private final InventoryDAO inventoryDAO;

    public ProductResource(ProductDAO productDAO, InventoryDAO inventoryDAO) {
        this.productDAO = productDAO;
        this.inventoryDAO = inventoryDAO;
    }

    /**
     * Get the product from the catalogue.
     * @param id Unique product identifier
     * @param shopId Shop identifier
     * @return Product details
     */
    @GET
    @Path("/{id}")
    public Response getProduct(@PathParam("id") String id, @QueryParam("shopId") String shopId) {
        Optional<Product> product = buildProduct(shopId, id);
        if (product.isPresent()) {
            return Response.ok(product.get()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    private Optional<Product> buildProduct(String shopId, String id) {
        Optional<Product> product = productDAO.getById(id);
        if (product.isPresent()) {
            product.get().setAvailableSizes(inventoryDAO.getAvailableSizes(shopId, id));
        }
        return product;
    }
}
