/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import tm470.ms.db.DeviceDAO;
import tm470.ms.db.DeviceDAOImpl;
import tm470.ms.db.InventoryDAO;
import tm470.ms.db.InventoryDAOImpl;
import tm470.ms.db.ProductDAO;
import tm470.ms.db.ProductDAOImpl;
import tm470.ms.db.TryOnDAO;
import tm470.ms.db.TryOnDAOImpl;
import tm470.ms.resources.AdminResource;
import tm470.ms.resources.DeviceResource;
import tm470.ms.resources.HeaderServerFilter;
import tm470.ms.resources.ProductResource;
import tm470.ms.resources.TryOnResource;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Starter class for BackMs
 */
public class BackMsApplication extends Application<BackMsConfiguration> {

    private ProductDAO productDAO;
    private InventoryDAO inventoryDAO;
    private DeviceDAO deviceDAO;
    private TryOnDAO tryOnDAO;

    public static void main(final String[] args) throws Exception {
        new BackMsApplication().run(args);
    }

    @Override
    public String getName() {
        return "BackMs";
    }

    @Override
    public void initialize(final Bootstrap<BackMsConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets", "/assets"));

        try {
            /* init mocked datastore */
            productDAO = new ProductDAOImpl(bootstrap.getObjectMapper(), "./products.json");
            inventoryDAO = new InventoryDAOImpl(bootstrap.getObjectMapper(), "./inventory.json");
            tryOnDAO = new TryOnDAOImpl();
            deviceDAO = new DeviceDAOImpl();

            /* init firebase */
            initFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void initFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("./firebase_private_key.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .build();

        FirebaseApp.initializeApp(options);
    }


    @Override
    public void run(final BackMsConfiguration configuration,
                    final Environment environment) {
        environment.jersey().register(new HeaderServerFilter());
        environment.jersey().register(new DeviceResource(deviceDAO));
        environment.jersey().register(new ProductResource(productDAO, inventoryDAO));
        environment.jersey().register(new AdminResource(inventoryDAO));
        environment.jersey().register(new TryOnResource(tryOnDAO, productDAO, deviceDAO));
    }

}
