/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tm470.ms.api.ItemAvailability;
import tm470.ms.api.Status;

import java.util.List;
import java.util.Optional;

/**
 * Build a push notification.
 * This class implements a Builder Pattern.
 */
public class NotificationBuilder {

    private final static Logger LOG = LoggerFactory.getLogger(NotificationBuilder.class);

    public static final String IN_PROGRESS_DEFAULT_MESSAGE =
            "Your request has been processed, it will take a few minutes.";
    public static final String DELIVERED_DEFAULT_MESSAGE =
            "Your shoes have been delivered to the pickup point.";
    public static final String REJECTED_DEFAULT_MESSAGE =
            "I'm sorry the shoes' sizes you have requested are no longer in stock.";

    private String id;
    private Status status;
    private Optional<List<ItemAvailability>> itemAvailabilities = Optional.empty();
    private String token;

    /**
     * Specify the ID of the TryOn request.
     * @param id TryOn request ID
     * @return This builder
     */
    public NotificationBuilder withId(String id) {
        this.id = id;
        return this;
    }

    /**
     * Specify the status of the request.
     * @param status Status
     * @return This builder
     */
    public NotificationBuilder withStatus(Status status) {
        this.status = status;
        return this;
    }

    /**
     * For each size given in the request, specify if it was available or not.
     * Only applicable when the request status is "DELIVERED".
     * @param itemAvailabilities Optional list of {@link ItemAvailability}
     * @return This builder
     */
    public NotificationBuilder withItemAvailabilities(Optional<List<ItemAvailability>> itemAvailabilities) {
        this.itemAvailabilities = itemAvailabilities;
        return this;
    }

    /**
     * Specify the Firebase token ID.
     * @param token Firebase token ID.
     * @return This builder
     */
    public NotificationBuilder withToken(String token) {
        this.token = token;
        return this;
    }

    /**
     * Build the Push notification.
     * @return The push notification just built.
     */
    public PushNotification build() {
        StringBuilder message = new StringBuilder();

        switch (status) {
            case IN_PROGRESS:
                message.append(IN_PROGRESS_DEFAULT_MESSAGE);
                break;
            case DELIVERED:
                message.append(DELIVERED_DEFAULT_MESSAGE);
                if (itemAvailabilities.isPresent()) {
                    for (ItemAvailability item : itemAvailabilities.get()) {
                        if (ItemAvailability.Availability.NOT_AVAILABLE.equals(item.getAvailability())) {
                            message.append(" Unfortunately size ").append(item.getSize()).append(" is no longer in stock.");
                        }
                    }
                }
                message.append(" Your id is ").append(id);
                break;
            case REJECTED:
                message.append(REJECTED_DEFAULT_MESSAGE);
                break;
        }

        return new PushNotification(token, message.toString());
    }
}
