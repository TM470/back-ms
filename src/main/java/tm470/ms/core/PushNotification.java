/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.core;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Push notification that needs to be sent by Google Firebase Cloud Messaging.
 */
public class PushNotification {

    private final static Logger LOG = LoggerFactory.getLogger(NotificationBuilder.class);
    private final String token;
    private final String preparedMessage;

    /**
     * Create a new push notification.
     * @param token Firebase token
     * @param preparedMessage Message that needs to be sent
     */
    PushNotification(String token, String preparedMessage) {
        this.token = token;
        this.preparedMessage = preparedMessage;
    }

    /**
     * Send push notification to the device identified by the given token
     * via Google Firebase Cloud Messaging.
     * @return If succesful return the notification receipt of notification.
     * @throws FirebaseMessagingException If the message cannot be delivered
     */
    public String send() throws FirebaseMessagingException {
        LOG.info("Sending to: " + token);
        LOG.info("Message: " + preparedMessage);

        if (null != token) {
            Message message = Message.builder()
                    .setNotification(new com.google.firebase.messaging.Notification("Shoes update", preparedMessage))
                    .setToken(token)
                    .build();

            try {
                return FirebaseMessaging.getInstance().send(message);
            } catch (FirebaseMessagingException e) {
                LOG.warn("Cannot send message to " + token, e);
            }
        } else {
            LOG.warn("Token not present! Cannot send notification");
        }
        return null;
    }

    public String getToken() {
        return token;
    }

    public String getPreparedMessage() {
        return preparedMessage;
    }
}
