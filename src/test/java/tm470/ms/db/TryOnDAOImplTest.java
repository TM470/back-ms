/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.BeforeClass;
import org.junit.Test;
import tm470.ms.api.Product;
import tm470.ms.api.Status;
import tm470.ms.api.TryOn;
import tm470.ms.resources.TryOnResource;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TryOnDAOImplTest {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    private static TryOnResource resource;
    private static ProductDAO productDAO;
    private static TryOnDAO tryOnDAO;
    private static DeviceDAO deviceDAO;

    @BeforeClass
    public static void init() {
        productDAO = new ProductDAOImpl(MAPPER, "./products.json");
        tryOnDAO = new TryOnDAOImpl();
        deviceDAO = new DeviceDAOImpl();
        resource = new TryOnResource(tryOnDAO, productDAO, deviceDAO);
    }

    @Test
    public void testAddTryOnAndGet() throws Exception {
        Product product = new Product("1234");

        TryOn tryOn = new TryOn();
        tryOn.setProduct(product);
        tryOn.setShopId("1");
        tryOn.setStatus(Status.INCOMING);
        tryOn.setSizes(Collections.singleton("10"));

        TryOn response = tryOnDAO.addTryOn(tryOn);

        TryOn tryOnAfter = tryOnDAO.getTryOn(response.getId());
        assertThat(tryOnAfter.getStatus(), is(Status.INCOMING));
        assertThat(tryOn.getProduct(), is(product));
        assertThat(tryOn.getShopId(), is("1"));
        assertThat(tryOn.getSizes(), hasItem("10"));
    }

    @Test
    public void testGetTryOns() throws Exception {
        TryOn tryOn = new TryOn();
        tryOn.setProduct(new Product("1234"));
        tryOn.setShopId("1");
        tryOn.setStatus(Status.INCOMING);
        tryOn.setSizes(Collections.singleton("10"));

        tryOnDAO.addTryOn(tryOn);
        assertTrue(tryOnDAO.getTryOns(Status.INCOMING).size() >= 1);
    }

    @Test
    public void testUpdateTryOnStatus() throws Exception {
        TryOn tryOn = new TryOn();
        tryOn.setProduct(new Product("1234"));
        tryOn.setShopId("1");
        tryOn.setStatus(Status.INCOMING);
        tryOn.setSizes(Collections.singleton("10"));

        tryOnDAO.addTryOn(tryOn);
        TryOn response = tryOnDAO.addTryOn(tryOn);

        tryOnDAO.updateTryOnStatus(response.getId(), Status.DELIVERED);
        assertThat(tryOnDAO.getTryOn(response.getId()).getStatus(), is(Status.DELIVERED));
    }
}