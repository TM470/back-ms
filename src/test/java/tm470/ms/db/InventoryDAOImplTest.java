/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.BeforeClass;
import org.junit.Test;
import tm470.ms.api.Size;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class InventoryDAOImplTest {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    private static InventoryDAO inventoryDAO;

    @BeforeClass
    public static void init() {
        inventoryDAO = new InventoryDAOImpl(MAPPER, "./inventory.json");
    }

    @Test
    public void testGetAvailableSizes() throws Exception {
        Set<Size> availableSizes = inventoryDAO.getAvailableSizes("1", "922908-007");
        assertThat(availableSizes, hasItem(new Size("9.5", 4)));
    }

    @Test
    public void testGetSizeQuantity() throws Exception {
        Optional<Integer> sizeQuantity = inventoryDAO.getSizeQuantity("1", "922908-007", "9.5");
        assertThat(sizeQuantity.isPresent(), is(true));
        assertThat(sizeQuantity.get(), is(4));
    }

    @Test
    public void testUpdateQuantity() throws Exception {
        inventoryDAO.updateQuantity("1", "922908-007", "10", 3);
        Optional<Integer> sizeQuantity = inventoryDAO.getSizeQuantity("1", "922908-007", "10");
        assertThat(sizeQuantity.isPresent(), is(true));
        assertThat(sizeQuantity.get(), is(3));

    }

    @Test
    public void testDeleteItem() throws Exception {
        inventoryDAO.deleteItem("1", "942851-006");
        assertThat(inventoryDAO.getAvailableSizes("1", "942851-006"), is(Collections.EMPTY_SET));
    }
}