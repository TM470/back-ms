/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessagingException;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.junit.Test;
import tm470.ms.BackMsApplication;
import tm470.ms.BackMsConfiguration;
import tm470.ms.api.Status;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PushNotificationTest {

    private static final String ID = "001";
    private static final Status STATUS = Status.INCOMING;
    private static final String TOKEN = "ABCD";

    @Test
    public void testGetters() throws Exception {
        NotificationBuilder notificationBuilder = new NotificationBuilder();
        notificationBuilder.withId(ID);
        notificationBuilder.withStatus(STATUS);
        notificationBuilder.withToken(TOKEN);

        PushNotification notification = notificationBuilder.build();
        assertThat(notification.getToken(), is(TOKEN));
    }

    @Test
    public void firebaseNotInitialised() throws FirebaseMessagingException {
        if (FirebaseApp.getInstance() == null) {
            Bootstrap<BackMsConfiguration> bootstrap = mock(Bootstrap.class);
            when(bootstrap.getObjectMapper()).thenReturn(new ObjectMapper());

            BackMsApplication backMsApplication = new BackMsApplication();
            backMsApplication.initialize(bootstrap);

            BackMsConfiguration backMsConfiguration = new BackMsConfiguration();

            Environment environment = mock(Environment.class);
            JerseyEnvironment jersey = mock(JerseyEnvironment.class);

            when(environment.jersey()).thenReturn(jersey);
            backMsApplication.run(backMsConfiguration, environment);
        }

        NotificationBuilder notificationBuilder = new NotificationBuilder();
        notificationBuilder.withId(ID);
        notificationBuilder.withStatus(STATUS);
        notificationBuilder.withToken(TOKEN);

        notificationBuilder.build().send();
    }
}