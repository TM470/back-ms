/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.core;

import org.junit.Test;
import tm470.ms.api.ItemAvailability;
import tm470.ms.api.Status;

import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class NotificationBuilderTest {

    private static final String ID = "001";
    private static final Status STATUS = Status.INCOMING;
    private static final String TOKEN = "ABCD";

    @Test
    public void testBuildInProgress() throws Exception {
        NotificationBuilder notificationBuilder = new NotificationBuilder();
        notificationBuilder.withId(ID);
        notificationBuilder.withStatus(Status.IN_PROGRESS);
        notificationBuilder.withToken(TOKEN);

        PushNotification notification = notificationBuilder.build();
        assertThat(notification.getPreparedMessage(), is(NotificationBuilder.IN_PROGRESS_DEFAULT_MESSAGE));
    }

    @Test
    public void testBuildRejected() throws Exception {
        NotificationBuilder notificationBuilder = new NotificationBuilder();
        notificationBuilder.withId(ID);
        notificationBuilder.withStatus(Status.REJECTED);
        notificationBuilder.withToken(TOKEN);


        PushNotification notification = notificationBuilder.build();
        assertThat(notification.getPreparedMessage(), is(NotificationBuilder.REJECTED_DEFAULT_MESSAGE));
    }

    @Test
    public void testBuildDelivered() throws Exception {
        NotificationBuilder notificationBuilder = new NotificationBuilder();
        notificationBuilder.withId(ID);
        notificationBuilder.withStatus(Status.DELIVERED);
        notificationBuilder.withToken(TOKEN);

        PushNotification notification = notificationBuilder.build();
        assertThat(notification.getPreparedMessage(), startsWith(NotificationBuilder.DELIVERED_DEFAULT_MESSAGE));
        assertThat(notification.getPreparedMessage(), containsString("Your id is " + ID));
    }

    @Test
    public void testBuildPartiallyDelivered() throws Exception {
        NotificationBuilder notificationBuilder = new NotificationBuilder();
        notificationBuilder.withId(ID);
        notificationBuilder.withStatus(Status.DELIVERED);
        notificationBuilder.withToken(TOKEN);

        ItemAvailability itemAvailability = new ItemAvailability("8", ItemAvailability.Availability.NOT_AVAILABLE);
        notificationBuilder.withItemAvailabilities(Optional.ofNullable(Collections.singletonList(itemAvailability)));

        PushNotification notification = notificationBuilder.build();
        assertThat(notification.getPreparedMessage(), startsWith(NotificationBuilder.DELIVERED_DEFAULT_MESSAGE));
        assertThat(notification.getPreparedMessage(), containsString("Your id is " + ID));
        assertThat(notification.getPreparedMessage(), containsString("Unfortunately size 8 is no longer in stock."));
    }
}