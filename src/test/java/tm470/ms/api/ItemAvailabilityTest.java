/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.api;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ItemAvailabilityTest {

    @Test
    public void testGetSize() throws Exception {
        ItemAvailability itemAvailability = new ItemAvailability("10",
                ItemAvailability.Availability.NOT_AVAILABLE);
        itemAvailability.setAvailability(ItemAvailability.Availability.AVAILABLE);
        assertThat(itemAvailability.getAvailability(), is(ItemAvailability.Availability.AVAILABLE));
    }

    @Test
    public void testGetAvailability() throws Exception {
        ItemAvailability itemAvailability = new ItemAvailability("10",
                ItemAvailability.Availability.NOT_AVAILABLE);
        itemAvailability.setSize("9");
        assertThat(itemAvailability.getSize(), is("9"));
    }
}