/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.BeforeClass;
import org.junit.Test;
import tm470.ms.api.Product;
import tm470.ms.api.Size;
import tm470.ms.db.DeviceDAO;
import tm470.ms.db.InventoryDAO;
import tm470.ms.db.InventoryDAOImpl;
import tm470.ms.db.ProductDAO;
import tm470.ms.db.ProductDAOImpl;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class ProductResourceTest {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    private static ProductResource resource;
    private static ProductDAO productDAO;
    private static InventoryDAO inventoryDAO;
    private static DeviceDAO deviceDAO;

    @BeforeClass
    public static void init() {
        productDAO = new ProductDAOImpl(MAPPER, "./products.json");
        inventoryDAO = new InventoryDAOImpl(MAPPER, "./inventory.json");
        resource = new ProductResource(productDAO, inventoryDAO);
    }

    @Test
    public void testGetProduct() throws Exception {
        Response response = resource.getProduct("AO9819-600", "1");
        assertThat(response.getStatus(), is(200));

        Product product = (Product) response.getEntity();
        assertThat(product.getId(), is("AO9819-600"));
        assertThat(product.getBrand(), is("NIKE"));
        assertThat(product.getName(), is("ODYSSEY REACT"));
        assertThat(product.getPrice().compareTo(new BigDecimal(104.5)), is(0));
        assertThat(product.getPhoto(), is("/assets/AO9819-600.png"));
        assertThat(product.getAvailableSizes(), hasItem(new Size("10", 1)));
        assertNull(product.getDescription());
    }
}