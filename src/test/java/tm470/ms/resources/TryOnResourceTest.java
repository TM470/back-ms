/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import tm470.ms.api.ItemAvailability;
import tm470.ms.api.Status;
import tm470.ms.api.TryOn;
import tm470.ms.api.TryOnRequest;
import tm470.ms.db.DeviceDAO;
import tm470.ms.db.DeviceDAOImpl;
import tm470.ms.db.ProductDAO;
import tm470.ms.db.ProductDAOImpl;
import tm470.ms.db.TryOnDAO;
import tm470.ms.db.TryOnDAOImpl;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TryOnResourceTest {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    private static TryOnResource resource;
    private static ProductDAO productDAO;
    private static TryOnDAO tryOnDAO;
    private static DeviceDAO deviceDAO;

    @BeforeClass
    public static void init() {
        productDAO = new ProductDAOImpl(MAPPER, "./products.json");
        tryOnDAO = new TryOnDAOImpl();
        deviceDAO = new DeviceDAOImpl();
        resource = new TryOnResource(tryOnDAO, productDAO, deviceDAO);
    }

    @Before
    public void setUp() throws Exception {
        TryOnRequest tryOnRequest = new TryOnRequest();
        tryOnRequest.setDeviceUUID("UUID1234");
        tryOnRequest.setSizes(Collections.singleton("10"));
        tryOnRequest.setProductId("AO9819-600");
        tryOnRequest.setShopId("1");

        resource.makeRequest(tryOnRequest);
    }

    @Test
    public void testMakeRequest() throws Exception {
        assertThat(resource.getRequests("001", "1", Status.INCOMING).get(0).getProduct().getId(), is("AO9819-600"));
    }

    @Test
    public void testGetRequests() throws Exception {
        List<TryOn> tryOns = resource.getRequests("001", "1", Status.INCOMING);
        assertThat(tryOns.get(0).getShopId(), is("1"));
        assertThat(tryOns.get(0).getId(), is("001"));
        assertTrue(tryOns.size() > 0);
    }

    @Test
    public void testChangeStatus() throws Exception {
        ItemAvailability availability = new ItemAvailability("10",
                ItemAvailability.Availability.NOT_AVAILABLE);

        resource.changeStatus("001", Status.DELIVERED, Collections.singletonList(availability));

        TryOn tryOn = tryOnDAO.getTryOn("001");

        assertThat(tryOn.getStatus(), is(Status.DELIVERED));
    }
}