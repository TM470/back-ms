/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import org.junit.BeforeClass;
import org.junit.Test;
import tm470.ms.db.DeviceDAO;
import tm470.ms.db.DeviceDAOImpl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DeviceResourceTest {

    private static DeviceResource resource;
    private static DeviceDAO deviceDAO;

    @BeforeClass
    public static void init() {
        deviceDAO = new DeviceDAOImpl();
        resource = new DeviceResource(deviceDAO);
    }

    @Test
    public void testRegister() throws Exception {
        resource.register("1234", "ABCD");
        assertThat(deviceDAO.getToken("1234"), is("ABCD"));
    }
}