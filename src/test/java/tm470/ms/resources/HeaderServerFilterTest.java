/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import org.junit.Test;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.core.MultivaluedMap;

import static org.mockito.Mockito.*;

public class HeaderServerFilterTest {

    @Test
    public void testFilter() throws Exception {
        HeaderServerFilter filter = new HeaderServerFilter();
        ContainerResponseContext contextResponse = mock(ContainerResponseContext.class);

        MultivaluedMap<String, Object> map = mock(MultivaluedMap.class);

        when(contextResponse.getHeaders()).thenReturn(map);

        filter.filter(mock(ContainerRequestContext.class), contextResponse);
        verify(map).add("Access-Control-Allow-Origin", "*");
        verify(map).add("Access-Control-Allow-Headers", "*");
        verify(map).add("Access-Control-Allow-Methods", "*");
    }
}