/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.BeforeClass;
import org.junit.Test;
import tm470.ms.db.InventoryDAOImpl;

import javax.ws.rs.core.Response;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AdminResourceTest {

    private final static ObjectMapper MAPPER = new ObjectMapper();
    private static InventoryDAOImpl inventoryDAO;
    private static AdminResource resource;

    private static final String PRODUCT_ID = "AO9819-600";
    private static final String SHOP_ID = "1";

    @BeforeClass
    public static void init() {
        inventoryDAO = new InventoryDAOImpl(MAPPER, "./inventory.json");
        resource = new AdminResource(inventoryDAO);
    }

    @Test
    public void testDecrementQuantity() throws Exception {
        Optional<Integer> sizeQuantity = inventoryDAO.getSizeQuantity(SHOP_ID, PRODUCT_ID, "10");
        assertThat(sizeQuantity.get(), is(1));

        resource.decrementQuantity(PRODUCT_ID, "10", SHOP_ID);

        Optional<Integer> sizeQuantityAfter = inventoryDAO.getSizeQuantity(SHOP_ID, PRODUCT_ID, "10");
        assertThat(sizeQuantityAfter.get(), is(0));
    }

    @Test
    public void testDecrementQuantityNotModified() throws Exception {
        Optional<Integer> sizeQuantity = inventoryDAO.getSizeQuantity(SHOP_ID, "942851-006", "10");
        assertThat(sizeQuantity.get(), is(1));

        resource.decrementQuantity(PRODUCT_ID, "10", SHOP_ID);
        Response response = resource.decrementQuantity(PRODUCT_ID, "10", SHOP_ID);//second time

        assertThat(response.getStatus(), is(304));
    }

    @Test
    public void testDecrementQuantityNotFound() throws Exception {
        Response response = resource.decrementQuantity(PRODUCT_ID, "5", SHOP_ID);
        assertThat(response.getStatus(), is(404));
    }

    @Test
    public void testDeleteItem() throws Exception {
        Optional<Integer> sizeQuantity = inventoryDAO.getSizeQuantity("2", "AO9819-600", "9.5");
        assertThat(sizeQuantity.get(), is(9));

        resource.deleteItem("AO9819-600", "2");

        Optional<Integer> sizeQuantityAfter = inventoryDAO.getSizeQuantity("2", "AO9819-600", "9.5");
        assertThat(sizeQuantityAfter, is(Optional.empty()));
    }
}