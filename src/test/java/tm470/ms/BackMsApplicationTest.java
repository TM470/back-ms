/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm470.ms;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class BackMsApplicationTest {

    private BackMsApplication backMsApplication;

    @Before
    public void init() {
        backMsApplication = new BackMsApplication();
    }

    @Test
    public void testGetName() throws Exception {
        assertThat(backMsApplication.getName(), is("BackMs"));
    }

    @Test
    public void testInitialize() throws Exception {
        Bootstrap<BackMsConfiguration> bootstrap = mock(Bootstrap.class);

        when(bootstrap.getObjectMapper()).thenReturn(new ObjectMapper());

        backMsApplication.initialize(bootstrap);
        verify(bootstrap).addBundle(any(AssetsBundle.class));
    }

    @Test
    public void testRun() throws Exception {
        BackMsConfiguration backMsConfiguration = new BackMsConfiguration();

        Environment environment = mock(Environment.class);
        JerseyEnvironment jersey = mock(JerseyEnvironment.class);

        when(environment.jersey()).thenReturn(jersey);
        backMsApplication.run(backMsConfiguration, environment);
        verify(jersey, times(5)).register(any(Object.class));
    }
}